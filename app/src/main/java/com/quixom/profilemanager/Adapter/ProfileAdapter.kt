package com.quixom.profilemanager.Adapter

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.quixom.profilemanager.AddProfileActivity
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Database.Profile
import com.quixom.profilemanager.Interface.PositionInterface
import com.quixom.profilemanager.R
import com.quixom.profilemanager.Receiver.MyAlarmReceiver
import com.quixom.profilemanager.Utils.GPSTracker
import com.quixom.profilemanager.Utils.Global
import kotlinx.android.synthetic.main.custom_profile_list.view.*
import java.util.*


/**
 * Created by Pooja on 14-12-2017.
 */
class ProfileAdapter(internal var context: Context, private var profileArrayList: ArrayList<Profile>,
                     private var database: MyDatabase) : RecyclerView.Adapter<ProfileAdapter.MyViewHolder>(),
        ActivityCompat.OnRequestPermissionsResultCallback {

    private var notificationManager: NotificationManager? = null
    private var permissionGranted: Boolean = false
    private var audioManager: AudioManager? = null
    private var AUDIO_REQUEST_CODE = 113
    private var ringerImage: Int? = null
    private var intent: Intent? = null
    private var serviceIntent: Intent? = null
    private var startHour: Int? = null
    private var startMin: Int? = null
    private var endHour: Int? = null
    private var endMin: Int? = null
    private var profile: Profile? = null
    private var alarmManager: AlarmManager? = null
    private var startTimeCalendar: Calendar? = null
    private var cancelTimeCalendar: Calendar? = null
    private var pendingIntent: PendingIntent? = null
    private var mMap: GoogleMap? = null
    private var switchChecked: Boolean? = null
    private var timeProfileId: Int? = 0
    private var colorCounter: Int = 0
    private var JOB_ID: Int? = null
    private val JOB_PERIODIC_TASK_TAG = "com.quixom.profilemanager.JobPeriodicTask"
    private var handler: Handler? = null
    private var gpsTracker: GPSTracker? = null

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.e("Profile Manager", "onBindViewHolder called!!!!")
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        serviceIntent = Intent(context, GPSTracker::class.java)
        intent = Intent(context, AddProfileActivity::class.java)
        profile = profileArrayList[position]
        val mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.itemView.recyclerViewSelectedDays.layoutManager = mLayoutManager
        holder.itemView.recyclerViewSelectedDays.itemAnimator = DefaultItemAnimator()
        alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        handler = Handler()
        gpsTracker = GPSTracker(context)

        holder.itemView.mapViewProfile.onCreate(null)
        holder.itemView.mapViewProfile.onResume()
        holder.itemView.mapViewProfile.getMapAsync({ googleMap: GoogleMap? ->
            run {
                mMap = googleMap
                MapsInitializer.initialize(context)
                /* mMap?.uiSettings?.isMapToolbarEnabled = false*/
                mMap?.uiSettings?.isZoomControlsEnabled = false
                if (profileArrayList[position].profileType == "Location") {
                    val latLng = LatLng(profileArrayList[position].profileLatitude!!, profileArrayList[position].profileLongitude!!)
                    mMap?.addMarker(MarkerOptions().position(latLng))
                    mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f))
                    mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json))
                }
            }
        })
        holder.itemView.mapViewProfile.postInvalidate()

        holder.itemView.textViewProfileName.text = profile!!.profileName

        //profile type-->time and location
        Log.e(Global.TAG, "profile type:::" + profile!!.profileType)
        if (profileArrayList[position].profileType == "Time") {
            holder.itemView.profileTime.visibility = View.VISIBLE
            holder.itemView.mapViewProfile.visibility = View.GONE
            val splitStartTime = profileArrayList[position].profileStartDate?.split(":")
            val splitEndTime = profileArrayList[position].profileEndDate?.split(":")
            holder.itemView.profileTime.text = """${(splitStartTime?.get(0)) + ":" + (splitStartTime?.get(1))} - ${(splitEndTime?.get(0)) + ":" + (splitEndTime?.get(1))}"""
        } else {
            holder.itemView.mapViewProfile.visibility = View.VISIBLE
            holder.itemView.profileTime.visibility = View.GONE
        }

        //No of days
        val selectedDays = database.profileDao().loadProfileById(profileArrayList[position].profileId!!)
        val fetchDaysById = selectedDays.profileDays

        holder.itemView.recyclerViewSelectedDays.adapter = DaysAdapter(context, database, Global.weekDaysList,
                fetchDaysById, "Adapter", object : PositionInterface {
            override fun getPositionArray(daysList: ArrayList<String>) {
                Log.e(Global.TAG, "Profile Adapter:Interface-days selected::" + daysList)
            }

            override fun getPositionHashMap(hashMap: HashMap<Int, String>) {
//                daysList = hashMap
            }
        })

        //ringer mode
        when (profile!!.profileRingerMode) {
            1 -> ringerImage = R.drawable.ic_volume_off
            2 -> ringerImage = R.drawable.ic_volume_up
            3 -> ringerImage = R.drawable.ic_vibration
        }
        holder.itemView.imageViewRingerMode.setImageResource(ringerImage!!)
        val ringerMode: Int? = when (ringerImage) {
            R.drawable.ic_volume_off -> AudioManager.RINGER_MODE_SILENT
            R.drawable.ic_volume_up -> AudioManager.RINGER_MODE_NORMAL
            R.drawable.ic_vibration -> AudioManager.RINGER_MODE_VIBRATE
            else -> AudioManager.RINGER_MODE_NORMAL
        }

        when (profileArrayList[position].profileStatus) {
            "On" -> {
                switchChecked = true
                if (profileArrayList[position].profileType == "Time") {
                    for (index in fetchDaysById!!.indices) {
                        val profileDays = profileArrayList[position].profileDays?.get(index)
                        var dayOfWeek: Int? = null
                        Log.e(Global.TAG, "Day of the week" + profileArrayList[position].profileDays?.get(index))
                        when (profileDays) {
                            "Sun" -> dayOfWeek = Calendar.SUNDAY
                            "Mon" -> dayOfWeek = Calendar.MONDAY
                            "Tue" -> dayOfWeek = Calendar.TUESDAY
                            "Wed" -> dayOfWeek = Calendar.WEDNESDAY
                            "Thu" -> dayOfWeek = Calendar.THURSDAY
                            "Fri" -> dayOfWeek = Calendar.FRIDAY
                            "Sat" -> dayOfWeek = Calendar.SATURDAY
                        }
                        startAlarm(position, ringerMode!!, dayOfWeek!!, holder)
                    }
                } else {
                    serviceIntent?.putExtra("profileId", profileArrayList[position].profileId!!)
                    context.startService(serviceIntent)
                    /*  gpsTracker?.getProfileLocation()
                      if (gpsTracker?.canGetLocation()!! && (gpsTracker?.getProfileLocation()?.latitude != 0.0 && gpsTracker?.getProfileLocation()?.longitude != 0.0)) {
                          context.startService(serviceIntent)
                      } else {
                          gpsTracker?.showSettingsAlert()
                      }*/
                }
            }
            "Off" -> {
                switchChecked = false
            }
        }

        holder.itemView.profileSwitch.isChecked = switchChecked as Boolean
        holder.itemView.profileSwitch.setOnCheckedChangeListener({ _, isChecked ->
            val onProfile = database.profileDao().getOnTimeProfiles("Time", "On")
            timeProfileId = if (onProfile != null) {
                onProfile.profileId!!
            } else {
                0
            }
            JOB_ID = profileArrayList[position].profileId
            if (profileArrayList[position].profileType == "Time" && timeProfileId != 0 && timeProfileId != profileArrayList[position].profileId) {
                val alertDialog = AlertDialog.Builder(context)
                alertDialog.setTitle("Warning!")
                        .setMessage("You cannot switch ON two time based profile at a time!")
                        .setCancelable(true)
                val dialog = alertDialog.create()
                dialog.show()
                holder.itemView.profileSwitch.isChecked = false
                database.profileDao().updateProfileStatus(profileArrayList[position].profileId, "Off")
            } else {
                if (isChecked) {
                    timeProfileId = profileArrayList[position].profileId!!
                    /*  if (isChecked == true) {*/
                    checkAudioPermission()
                    if (permissionGranted) {
                        Log.e("Profile Manager", "Position==" + position)

                        if (profileArrayList[position].profileType == "Time") {

                            /*val jobScheduler = SmartScheduler.getInstance(context)

                            if (!jobScheduler.contains(JOB_ID!!)) {
                                val job = createJob(position)
                                jobScheduler.addJob(job)
                            }*/

                            for (index in fetchDaysById!!.indices) {
                                val profileDays = profileArrayList[position].profileDays?.get(index)
                                var dayOfWeek: Int? = null
                                Log.e(Global.TAG, "Day of the week" + profileArrayList[position].profileDays?.get(index))
                                when (profileDays) {
                                    "Sun" -> dayOfWeek = Calendar.SUNDAY
                                    "Mon" -> dayOfWeek = Calendar.MONDAY
                                    "Tue" -> dayOfWeek = Calendar.TUESDAY
                                    "Wed" -> dayOfWeek = Calendar.WEDNESDAY
                                    "Thu" -> dayOfWeek = Calendar.THURSDAY
                                    "Fri" -> dayOfWeek = Calendar.FRIDAY
                                    "Sat" -> dayOfWeek = Calendar.SATURDAY
                                }
                                startAlarm(position, ringerMode!!, dayOfWeek!!, holder)
                            }
                        } else {
                            serviceIntent?.putExtra("profileId", profileArrayList[position].profileId!!)
//                            context.startService(serviceIntent)
                            gpsTracker?.getProfileLocation()
                            if (gpsTracker?.canGetLocation()!!) {
                                context.startService(serviceIntent)
                            } else {
                                gpsTracker?.showSettingsAlert()
                            }
                        }
//                }
                        database.profileDao().updateProfileStatus(profileArrayList[position].profileId, "On")
                        holder.itemView.profileSwitch.isChecked = true
                    }
                } else {
                    timeProfileId = 0
                    database.profileDao().updateProfileStatus(profileArrayList[position].profileId, "Off")
                    holder.itemView.profileSwitch.isChecked = false
                    if (profileArrayList[position].profileType == "Time") {

                        /* val smartScheduler = SmartScheduler.getInstance(context)
                         smartScheduler.removeJob(JOB_ID!!)*/
                        alarmManager?.cancel(pendingIntent)
                        audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    } else {
                        audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                        context.stopService(serviceIntent)
                    }
                }
            }
        })

        if (profileArrayList[position].profileType == "Time") {
            if (colorCounter < 4) {
                holder.itemView.profileTime.setBackgroundResource(Global.getColor(colorCounter))
                colorCounter += 1
            } else {
                colorCounter = 0
                holder.itemView.profileTime.setBackgroundResource(Global.getColor(colorCounter))
                colorCounter += 1
            }
        }
        holder.itemView.cardView.setOnClickListener(
                {
                    intent?.putExtra("profileId", profileArrayList[position].profileId!!)
                    intent?.putExtra("comeFrom", "Edit")
                    intent?.putExtra("profileType", profileArrayList[position].profileType)
                    context.startActivity(intent)
                })
    }

    override fun getItemCount(): Int = profileArrayList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        Log.e("Profile Manager", "onCreateViewHolder called!!!!!!")
        val v = LayoutInflater.from(parent.context).inflate(R.layout.custom_profile_list, parent, false)
        return MyViewHolder(v)
    }

    /* private fun getTimeDifference(startTime: String?, endTime: String?): Long {
         val format = SimpleDateFormat("HH:mm:ss a", Locale.US)
         val profileStartTime = format.parse(startTime)
         val profileEndTime = format.parse(endTime)
         *//*val hours = millis / (1000 * 60 * 60)
        val mins = millis % (1000 * 60 * 60)
        println("Hours::" + Math.abs(hours) + " Mins::" + mins)*//*
        return profileStartTime.time - profileEndTime.time
    }*/

    /* private fun createJob(position: Int): Job {
         val jobType = Job.Type.JOB_TYPE_HANDLER
         val intervalInMillis = timeDifference
         val builder = Job.Builder(JOB_ID!!, this, jobType, JOB_PERIODIC_TASK_TAG)
 //                .setIntervalMillis(Math.abs(intervalInMillis!!))

         val splitStartTime = profileArrayList.get(position).profileStartDate!!.split(":")
         val splitEndTime = profileArrayList.get(position).profileEndDate!!.split(":")
         Log.e(Global.TAG, "first split::" + splitStartTime[0].toInt() + "second split::" + splitStartTime[1] + "third split" + splitStartTime!![2])
         startHour = splitStartTime[0].toInt()
         startMin = splitStartTime[1].toInt()
         val startSecond = splitStartTime[2].split(" ")
         Log.e(Global.TAG, "start second:::" + startSecond[0])

         endHour = splitEndTime[0].toInt()
         endMin = splitEndTime[1].toInt()
         val endSecond = splitEndTime[2].split(" ")
         Log.e(Global.TAG, "start second:::" + endSecond[0])

         startTimeCalendar = Calendar.getInstance()
         startTimeCalendar!!.timeInMillis = System.currentTimeMillis()
         startTimeCalendar!!.set(Calendar.HOUR_OF_DAY, startHour!!)
         startTimeCalendar!!.set(Calendar.MINUTE, startMin!!)
         startTimeCalendar!!.set(Calendar.SECOND, startSecond[0].toInt())

         cancelTimeCalendar = Calendar.getInstance()
         cancelTimeCalendar!!.timeInMillis = System.currentTimeMillis()
         cancelTimeCalendar!!.set(Calendar.HOUR_OF_DAY, endHour!!)
         cancelTimeCalendar!!.set(Calendar.MINUTE, endMin!!)
         cancelTimeCalendar!!.set(Calendar.SECOND, endSecond[0].toInt())

         val delay = Math.abs(System.currentTimeMillis() - startTimeCalendar?.timeInMillis!!)
         if (System.currentTimeMillis() < startTimeCalendar?.timeInMillis!!) {
             builder.setPeriodic(Math.abs(intervalInMillis!!), delay)
         } else {
             if (System.currentTimeMillis() < cancelTimeCalendar?.timeInMillis!!) {
                 builder.setPeriodic(Math.abs(intervalInMillis!!), 0)
             }
         }

         Log.e(Global.TAG, "job created successfully!!!")
         return builder.build()
     }*/

    /*  private fun endJob(position: Int): Job {
          val jobType = Job.Type.JOB_TYPE_HANDLER
          val intervalInMillis = timeDifference
          val builder = Job.Builder(JOB_ID!!, this, jobType, JOB_PERIODIC_TASK_TAG)
          val endTime = database.profileDao().getEndTime(position)
  //        val splitStartTime = endTime?.split(":")
          val splitEndTime = endTime?.split(":")
          endHour = splitEndTime!![0].toInt()
          endMin = splitEndTime[1].toInt()
          val endSecond = splitEndTime[2].split(" ")
          cancelTimeCalendar = Calendar.getInstance()
          cancelTimeCalendar!!.timeInMillis = System.currentTimeMillis()
          cancelTimeCalendar!!.set(Calendar.HOUR_OF_DAY, endHour!!)
          cancelTimeCalendar!!.set(Calendar.MINUTE, endMin!!)
          cancelTimeCalendar!!.set(Calendar.SECOND, endSecond[0].toInt())

          val delay = Math.abs(System.currentTimeMillis() - cancelTimeCalendar?.timeInMillis!!)
          builder.setPeriodic(intervalInMillis!!, delay)

          return builder.build()
      }*/

    /* override fun onJobScheduled(context: Context?, job: Job?) {
         if (job != null) {
             Log.e(Global.TAG, "onJobScheduled called!!!!!!")
             if (startTimeCalendar?.timeInMillis!! < System.currentTimeMillis()) {

                 if (cancelTimeCalendar?.timeInMillis!! < System.currentTimeMillis()) {
                     audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                     val smartScheduler = SmartScheduler.getInstance(context)
                     smartScheduler.removeJob(JOB_ID!!)
                 } else {
                     audioManager?.ringerMode = profile?.profileRingerMode!! - 1
                     val smartScheduler = SmartScheduler.getInstance(context)
                     smartScheduler.removeJob(JOB_ID!!)
                     val jobScheduler = SmartScheduler.getInstance(context)

                     if (!jobScheduler.contains(JOB_ID!!)) {
                         val endJob = endJob(this.timeProfileId!!)
                         jobScheduler.addJob(endJob)
                     }
                 }
             }
         }
     }*/

    /* private fun setUpAlarms(position: Int, ringerMode: Int, dayOfWeek: Int, holder: MyViewHolder) {
         startAlarm(position, ringerMode, dayOfWeek, holder)
     }*/

    private fun startAlarm(position: Int, ringerMode: Int, dayOfWeek: Int, holder: MyViewHolder) {

        val splitStartTime = profileArrayList[position].profileStartDate!!.split(":")
        startHour = splitStartTime[0].toInt()
        startMin = splitStartTime[1].toInt()
        val startSecond = splitStartTime[2].split(" ")

        val splitEndTime = profileArrayList[position].profileEndDate!!.split(":")
        endHour = splitEndTime[0].toInt()
        endMin = splitEndTime[1].toInt()
        val endSecond = splitEndTime[2].split(" ")

        /*  val date = Date()
          val calendarNow = Calendar.getInstance()*/
        startTimeCalendar = Calendar.getInstance()
        /*calendarNow.time = date
        startTimeCalendar?.time = date*/
        startTimeCalendar?.timeInMillis = System.currentTimeMillis()
        Log.e(Global.TAG, "Day of the week:::" + dayOfWeek)
        startTimeCalendar?.set(Calendar.DAY_OF_WEEK, dayOfWeek)
        startTimeCalendar?.set(Calendar.HOUR_OF_DAY, startHour!!)
        startTimeCalendar?.set(Calendar.MINUTE, startMin!!)
        startTimeCalendar?.set(Calendar.SECOND, startSecond[0].toInt())

        /*  if (startTimeCalendar!!.before(calendarNow)) {//if its in the past increment
              startTimeCalendar?.add(Calendar.DATE, 1)
          }*/

        /* if (startTimeCalendar?.timeInMillis!! < System.currentTimeMillis()) {
             startTimeCalendar?.add(Calendar.DAY_OF_MONTH, GregorianCalendar().get(Calendar.DAY_OF_WEEK) - 1)
         }*/

        cancelTimeCalendar = Calendar.getInstance()
        cancelTimeCalendar?.timeInMillis = System.currentTimeMillis()
//        cancelTimeCalendar?.set(Calendar.DAY_OF_WEEK, dayOfWeek)
        cancelTimeCalendar?.set(Calendar.HOUR_OF_DAY, endHour!!)
        cancelTimeCalendar?.set(Calendar.MINUTE, endMin!!)
        cancelTimeCalendar?.set(Calendar.SECOND, endSecond[0].toInt())

        val intent = Intent(context, MyAlarmReceiver::class.java)
        intent.putExtra("ringerMode", ringerMode)
        intent.putExtra("startTimeCalendar", startTimeCalendar!!.timeInMillis)
        intent.putExtra("cancelTimeCalendar", cancelTimeCalendar!!.timeInMillis)
        pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        if (startTimeCalendar?.timeInMillis!! > System.currentTimeMillis()) {
            alarmManager?.setInexactRepeating(AlarmManager.RTC_WAKEUP, startTimeCalendar?.timeInMillis!!, AlarmManager.INTERVAL_DAY, pendingIntent)
        } else {
            if (System.currentTimeMillis() < cancelTimeCalendar?.timeInMillis!!) {
                alarmManager?.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_DAY, pendingIntent)
            } else {
                alarmManager?.cancel(pendingIntent)
                pendingIntent?.cancel()
                holder.itemView.profileSwitch.isSelected = false
                Log.e(Global.TAG, "Cancel time < System time!!!!!!")
            }
        }

        checkForAlarm()
    }


    private fun checkForAlarm() {
        val intent = Intent(context, MyAlarmReceiver::class.java)
        intent.action = MyAlarmReceiver.ACTION_ALARM_RECEIVER
        val isWorking = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_NO_CREATE) != null//just changed the flag
        Log.e(Global.TAG, "alarm is " + (if (isWorking) "" else "not") + " working...")
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkAudioPermission() {
        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (notificationManager!!.isNotificationPolicyAccessGranted) {
                Log.e("Profile Manager", "Notification Policy access::" +
                        notificationManager!!.isNotificationPolicyAccessGranted)
                permissionGranted = true
            } else {
                // Ask the user to grant access
                val intent = Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS)
                context.startActivity(intent)
            }
        } else {
            permissionGranted = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.e("PROFILE MANAGER", "onRequestPermissionsResult called!!!")
        when (requestCode) {

            AUDIO_REQUEST_CODE -> {
                Log.e("Profile Manager", "Grant Results::::" + grantResults)
                Log.e("Profile Manager", "Airplane Permission Granted size==" + grantResults[0] +
                        " Package Manager--" + PackageManager.PERMISSION_GRANTED)
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    /*var isEnabled = Settings.System.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1
                    Settings.System.putInt(context.getContentResolver(),
                            Settings.Global.AIRPLANE_MODE_ON, 1)
                     var newIntent = Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED)
                     newIntent.putExtra("state", true)
                     context.sendBroadcast(newIntent)*/
                } else {
                    Log.e("PROFILE MANAGER", "Audio Grant Results size::" + grantResults.size)
                }
            }
        }
    }

}