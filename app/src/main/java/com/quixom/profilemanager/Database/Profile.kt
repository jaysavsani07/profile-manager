package com.quixom.profilemanager.Database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Pooja on 28-12-2017.
 */
@Entity
class Profile {

    @PrimaryKey(autoGenerate = true)
    var profileId: Int? = null

    @ColumnInfo(name = "profile_name")
    var profileName: String? = ""

    @ColumnInfo(name = "profile_startDate")
    var profileStartDate: String? = ""

    @ColumnInfo(name = "profile_endDate")
    var profileEndDate: String? = ""

    @ColumnInfo(name = "profile_location")
    var profileLocation: String? = ""

    @ColumnInfo(name = "profile_latitude")
    var profileLatitude: Double? = null

    @ColumnInfo(name = "profile_longitude")
    var profileLongitude: Double? = null

    @ColumnInfo(name = "profile_locationRadius")
    var profileLocationRadius: String? = ""

    @ColumnInfo(name = "profile_days")
    var profileDays: ArrayList<String>? = null

    @ColumnInfo(name = "profile_ringerMode")
    var profileRingerMode: Int? = null

    @ColumnInfo(name = "profile_type")
    var profileType: String? = ""

    @ColumnInfo(name = "profile_status")
    var profileStatus: String? = ""

}