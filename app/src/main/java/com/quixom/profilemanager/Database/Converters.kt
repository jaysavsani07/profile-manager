package com.quixom.profilemanager.Database

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Pooja on 09-03-2018.
 */
class Converters {

    @TypeConverter
    fun fromStringToArrayList(value: String): ArrayList<String> {
        val listType = object : TypeToken<ArrayList<String>>() {

        }.type
        return Gson().fromJson(value, listType)
        // return value == null ? null : new Date(value);
    }


    @TypeConverter
    fun arrayListToString(list: ArrayList<String>): String {
        val gson = Gson()

        return gson.toJson(list)
    }

}