package com.quixom.profilemanager.Receiver

import android.app.AlarmManager
import android.app.PendingIntent
import android.arch.persistence.room.Room
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.util.Log
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Utils.Global


/**
 * Created by Pooja on 28-02-2018.
 */
class MyAlarmReceiver() : BroadcastReceiver() {

    companion object {
        var ringerMode: Int? = null
        var startTime: Long? = null
        var cancelTime: Long? = null
        var audioManager: AudioManager? = null
        var alarmManager: AlarmManager? = null
        private var pendingIntent: PendingIntent? = null
        var database: MyDatabase? = null
        var ACTION_ALARM_RECEIVER: String? = null
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        ACTION_ALARM_RECEIVER = "ACTION_ALARM_RECEIVER"
        alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        ringerMode = intent?.extras?.getInt("ringerMode")
        startTime = intent?.extras?.getLong("startTimeCalendar")
        cancelTime = intent?.extras?.getLong("cancelTimeCalendar")
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        database = Room.databaseBuilder(context, MyDatabase::class.java, "profile_database").allowMainThreadQueries().build()
        /* val daysList = database?.profileDao()?.getOnTimeProfiles("Time", "On")?.profileDays
         Log.e(Global.TAG, "Receiver::::" + daysList + " current day of week:::" + Global.getCurrentDayOfWeek())*/
        if (System.currentTimeMillis() >= cancelTime!!) {
            Log.e(Global.TAG, "Your time is up!!!")
            val cancelIntent = Intent(context, MyAlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(context, 2, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
            alarmManager?.cancel(pendingIntent)
        } else {
            Log.e(Global.TAG, "Alarm Started at::" + System.currentTimeMillis())
            Log.e(Global.TAG, "Intent data::" + intent?.extras?.getInt("ringerMode"))
            audioManager!!.ringerMode = ringerMode!!
            val startIntent = Intent(context, MyAlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(context, 1, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            alarmManager?.cancel(pendingIntent)

            val endIntent = Intent(context, MyAlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(context, 2, endIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            alarmManager?.setInexactRepeating(AlarmManager.RTC_WAKEUP, cancelTime!!, AlarmManager.INTERVAL_DAY * 7, pendingIntent)
        }
    }

}

