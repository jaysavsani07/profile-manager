package com.quixom.profilemanager.Database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

/**
 * Created by Pooja on 28-12-2017.
 */
@Dao
interface ProfileDao {

    @Query("SELECT * FROM Profile")
    fun getAllProfile(): List<Profile>

    @Query("SELECT * FROM Profile where profileId = :arg0")
    fun loadProfileById(profileId: Int): Profile

    @Query("UPDATE Profile SET profile_name=:arg1,profile_startDate=:arg2,profile_endDate=:arg3," +
            "profile_location=:arg4,profile_latitude=:arg5,profile_longitude=:arg6," +
            "profile_locationRadius=:arg7,profile_days=:arg8,profile_ringerMode=:arg9,profile_type=:arg10,profile_status=:arg11 WHERE profileId = :arg0")
    fun updateProfile(profileId: Int?, profileName: String?, profileStartTime: String?, profileEndTime: String?, profileLocation: String?,
                      profileLatitude: Double?, profileLongitude: Double?, profileRadius: String?, profileDays: ArrayList<String>?, profileRingerMode: Int?,
                      profileType: String?, profileStatus: String?)

    @Query("DELETE FROM Profile WHERE profileId=:arg0")
    fun deleteProfileById(position: Int)

    @Query("UPDATE Profile SET profile_status=:arg1 WHERE profileId=:arg0")
    fun updateProfileStatus(profileId: Int?, profileStatus: String?)

    @Query("SELECT * FROM Profile WHERE profile_status=:arg1 and profile_type=:arg0")
    fun getOnTimeProfiles(profileType: String?, profileStatus: String?): Profile

    @Query("SELECT * FROM Profile WHERE profile_status=:arg1 and profile_type=:arg0")
    fun getOnLocationProfiles(profileType: String?,profileStatus: String?): List<Profile>

    @Query("SELECT profile_endDate FROM Profile WHERE profileId=:arg0")
    fun getEndTime(profileId: Int?): String?

    @Insert(onConflict = REPLACE)
    fun insertProfile(profile: Profile)
}