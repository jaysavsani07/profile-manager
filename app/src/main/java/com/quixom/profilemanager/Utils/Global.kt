package com.quixom.profilemanager.Utils

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.quixom.profilemanager.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Pooja on 13-12-2017.
 */
public class Global {

    companion object {
         var TAG = "PROFILE MANAGER"

         var weekDaysList = arrayListOf("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")

        fun getCurrentDayOfWeek(): String {
            val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())
            val sdf = SimpleDateFormat("EEE", Locale.US)
            val date = Date()
            val week = sdf.format(date)
            Log.e("Profile Manager", "current date and time::$currentDateTimeString week::$week")
            return week
        }

        fun hideKeyboard(activity: Activity) {

            activity.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            val view: View = activity.currentFocus
            if (true) run {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }


        fun getColor(colorCounter: Int): Int = when (colorCounter) {

            0 -> R.color.cardBlue
            1 -> R.color.cardGreen
            2 -> R.color.cardMagenta
            3 -> R.color.cardSeeGreen
            else -> R.color.cardMagenta
        }


    }
}