package com.quixom.profilemanager.Interface

/**
 * Created by Pooja on 16-12-2017.
 */
interface PositionInterface {

   fun getPositionHashMap(hashMap: HashMap<Int,String>)
   fun getPositionArray(daysList:ArrayList<String>)
}