package com.quixom.profilemanager.Utils

import android.app.Service
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.location.Location
import android.media.AudioManager
import android.os.*
import android.os.Process.THREAD_PRIORITY_BACKGROUND
import android.util.Log
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Database.Profile
import java.util.*

class MyLocationService : Service() {

    class ServiceHandler(looper: Looper) : Handler() {

        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)

        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.e("Profile Manager", "onCreate called!!!!!!!!")
        val thread = HandlerThread("ServiceStartArguments",
                THREAD_PRIORITY_BACKGROUND)
        thread.start()

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.looper
        mServiceHandler = ServiceHandler(mServiceLooper!!)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.e("Profile Manager", "onStartCommand called!!!!!!!")
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        val msg = mServiceHandler?.obtainMessage()
        msg?.arg1 = startId
        mServiceHandler?.sendMessage(msg)

//        gpsTracker = GPSTracker(this)
        database = Room.databaseBuilder(this, MyDatabase::class.java, "profile_database").allowMainThreadQueries().build()
        profile = Profile()
        if (intent != null) {
            profileId = intent.extras.getInt("profileId")
            ringerMode = intent.extras.getInt("ringerMode")
        }

        audioManager = this.getSystemService(Context.AUDIO_SERVICE) as AudioManager?

        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {

            override fun run() {
                GpsTrackerCredentials().execute()
            }
        }, 0, (5 * 1000).toLong())

        // If we get killed, after returning from here, restarts
        return Service.START_STICKY
    }

    companion object {
        private var timer: Timer? = null
        var gpsTracker: GPSTracker? = null
        private var mServiceLooper: Looper? = null
        private var mServiceHandler: ServiceHandler? = null
        var database: MyDatabase? = null
        private var profile: Profile? = null
        var profileId: Int? = null
        var audioManager: AudioManager? = null
        var ringerMode: Int? = null


        private class GpsTrackerCredentials : AsyncTask<String?, String?, String?>() {

            override fun doInBackground(vararg arg0: String?): String? {
                return null
            }

            override fun onPostExecute(unused: String?) {
                super.onPostExecute(unused)
                if (gpsTracker?.getProfileLocation()?.latitude == 0.0 || gpsTracker?.getProfileLocation()?.longitude == 0.0) {
                    gpsTracker?.getProfileLocation()
                    if (gpsTracker?.canGetLocation()!!) {
                        if (profile != null && profileId != null) {
                            profile = database?.profileDao()?.loadProfileById(profileId!!)

                            val dist = FloatArray(1)
                            Location.distanceBetween(profile?.profileLatitude!!, profile?.profileLongitude!!, gpsTracker?.getProfileLocation()?.latitude!!,
                                    gpsTracker?.getProfileLocation()?.longitude!!, dist)
                            Log.e(Global.TAG, "Distance between::" + dist[0] / 1000)
                            val radius = "0." + profile?.profileLocationRadius
                            if (dist[0] / 1000 < radius.toFloat()) {
                                Log.e(Global.TAG, "You are into the profile location!")
                                when (ringerMode) {
                                    0 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT
                                    1 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                                    2 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                                    else -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                                }
                            } else {
                                /*val onTimeProfile = database?.profileDao()?.getOnTimeProfiles("Time", "On")
                                val timeProfile = onTimeProfile?.profileId*/

                                audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL

                                Log.e(Global.TAG, "You are out of the profile location!")
                            }
                        }
                    }
                } else {
                    if (profile != null && profileId != null) {
                        profile = database?.profileDao()?.loadProfileById(profileId!!)

                        val dist = FloatArray(1)
                        Location.distanceBetween(profile?.profileLatitude!!, profile?.profileLongitude!!, gpsTracker?.getProfileLocation()?.latitude!!,
                                gpsTracker?.getProfileLocation()?.longitude!!, dist)
                        Log.e(Global.TAG, "Distance between::" + dist[0] / 1000)
                        val radius = "0." + profile?.profileLocationRadius
                        if (dist[0] / 1000 < radius.toFloat()) {
                            Log.e(Global.TAG, "You are into the profile location!")
                            when (ringerMode) {
                                0 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT
                                2 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                                1 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                                else -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                            }
                        } else {
                           /* val onTimeProfile = database?.profileDao()?.getOnTimeProfiles("Time", "On")
                            val timeProfile = onTimeProfile?.profileId*/

                            audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL

                            Log.e(Global.TAG, "You are out of the profile location!")
                        }
                    }
                }
            }
        }
    }

    /*fun geofence()
   {
       var geofence = Geofence.Builder()
   .setRequestId(GEOFENCE_REQ_ID) // Geofence ID
   .setCircularRegion( LATITUDE, LONGITUDE, RADIUS) // defining fence region
   .setExpirationDuration( DURANTION ) // expiring date
   // Transition types that it should look for
   .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
   .build()
   }*/

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Profile Manager", "Service is stopped!!!!")
        timer?.cancel()
        timer = null
        mServiceLooper?.quit()

        GpsTrackerCredentials().cancel(true)
        gpsTracker?.stopUsingGPS()
        database?.profileDao()?.updateProfileStatus(profileId, "Off")
        stopSelf()
    }


}
