package com.quixom.profilemanager.Receiver

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.quixom.profilemanager.Utils.Global


/**
 * Created by Pooja on 28-02-2018.
 */
class DeviceBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == "android.intent.action.BOOT_COMPLETED") {

            Log.e(Global.TAG, "Received boot completed")
            val alarmIntent = Intent(context, MyAlarmReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent,  PendingIntent.FLAG_CANCEL_CURRENT)

            /*val manager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val interval = 2000
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval.toLong(), pendingIntent)

            Toast.makeText(context, "Alarm Set", Toast.LENGTH_SHORT).show()*/
        }
    }
}