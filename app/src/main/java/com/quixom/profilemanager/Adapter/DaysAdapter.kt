package com.quixom.profilemanager.Adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Interface.PositionInterface
import com.quixom.profilemanager.R
import com.quixom.profilemanager.Utils.Global
import kotlinx.android.synthetic.main.custom_profile_list.view.*
import kotlinx.android.synthetic.main.days_list.view.*


/**
 * Created by Pooja on 15-12-2017.
 */
class DaysAdapter(internal var context: Context, internal var database: MyDatabase, private var daysList: ArrayList<String>,
                  private var selectedDaysPos: ArrayList<String>? = null, private var comeFrom: String,
                  private var positionInterface: PositionInterface) : RecyclerView.Adapter<DaysAdapter.MyViewHolder>() {

    private var hashMap: HashMap<Int, String>? = HashMap()
    private var day: String? = null
    private var change: Int? = null
    private var daysArrayList: ArrayList<String>? = ArrayList()
    private var selectedArrayList: HashMap<Int, String>? = null
    private var tempHashMap: HashMap<Int, String>? = null

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val profileDay: String = daysList[position]
        val firstChar = profileDay.take(1)

        selectedArrayList = HashMap()
        tempHashMap = HashMap()

        if (selectedDaysPos != null) {

            if (comeFrom == "Adapter") {
                holder.itemView?.linearLayoutProfileDays?.removeAllViews()
                val layoutParams = LinearLayout.LayoutParams(80, 80)
                layoutParams.setMargins(10, 10, 10, 10)
                holder.itemView?.profileDay?.layoutParams = layoutParams
                holder.itemView?.linearLayoutProfileDays?.addView(holder.itemView.profileDay)
            }

            for (i in selectedDaysPos!!.indices) {
                /*unselectedArrayList!!.add(selectedDaysPos!!.get(i))*/
                var index: Int? = null
                when (selectedDaysPos?.get(i)) {
                    "Sun" -> index = 0
                    "Mon" -> index = 1
                    "Tue" -> index = 2
                    "Wed" -> index = 3
                    "Thu" -> index = 4
                    "Fri" -> index = 5
                    "Sat" -> index = 6
                }
                tempHashMap?.put(index!!, selectedDaysPos!![i])
            }

            /* for (i in selectedDaysPos!!.indices) {
                tempHashMap?.put(position, selectedArrayList!!.get(position)!!)
            }*/

            //if (selectedDaysPos!!.getOrNull(position) != null) {

            Log.e(Global.TAG, "days :::" + daysList[position])
            Log.e(Global.TAG, "Position :::" + tempHashMap?.get(position))

            //val selectedDayFirstChar = selectedDaysPos!!.getOrNull(position)!!.profileDays!!.take(1)
            if (daysList[position] == tempHashMap?.get(position)) {
                selectedArrayList?.put(position, daysList[position])
                daysArrayList?.add(daysList[position])
                Log.e(Global.TAG, "selected days list::" + selectedArrayList?.get(position))
                holder.itemView?.profileDay?.tag = 0
                holder.itemView?.profileDay?.setBackgroundResource(R.drawable.button_solid)
                holder.itemView?.profileDay?.setTextColor(Color.WHITE)
                holder.itemView?.profileDay?.text = daysList[position].take(1)
            } else {
                holder.itemView?.profileDay?.tag = 1
                holder.itemView?.profileDay?.text = firstChar
            }
//            } else {
//
//            }
        } else {
            holder.itemView?.profileDay?.tag = 1
            holder.itemView?.profileDay?.text = firstChar
            Log.e("PROFILE MANAGER", " Tag:::" + holder.itemView?.profileDay?.tag as Int)
        }

        if (comeFrom != "Adapter") {
            Log.e(Global.TAG, "come from::::" + comeFrom)
            holder.itemView?.profileDay?.setOnClickListener({
                Log.e("PROFILE MANAGER", "POSITION:::" + position)
                change = (holder.itemView.profileDay?.tag as Int?)!!
                Log.e("PROFILE MANAGER", "Button Clicked!!!!  Tag:::" + holder.itemView.profileDay?.tag)

                if (change == 1) {
                    holder.itemView.profileDay?.setBackgroundResource(R.drawable.button_solid)
                    holder.itemView.profileDay?.setTextColor(Color.WHITE)
                    holder.itemView.profileDay?.tag = 0

                    when (position) {
                        0 -> day = "Sun"
                        1 -> day = "Mon"
                        2 -> day = "Tue"
                        3 -> day = "Wed"
                        4 -> day = "Thu"
                        5 -> day = "Fri"
                        6 -> day = "Sat"
                    }
                    if (comeFrom == "Edit") {
                        daysArrayList?.add(daysList[position])
                    } else {
                        hashMap?.put(position, day!!)
                        daysArrayList?.add(daysList[position])
                    }
                    positionInterface.getPositionHashMap(hashMap!!)
                    positionInterface.getPositionArray(daysArrayList!!)
                } else {

                    holder.itemView.profileDay?.setBackgroundResource(R.drawable.button_border)
                    holder.itemView.profileDay?.setTextColor(Color.WHITE)
                    holder.itemView.profileDay?.tag = 1
                    if (comeFrom == "Edit") {
                        daysArrayList?.remove(daysList[position])
                    } else {
                        hashMap?.remove(position)
                        daysArrayList?.remove(daysList[position])
                    }
                    positionInterface.getPositionHashMap(hashMap!!)
                    positionInterface.getPositionArray(daysArrayList!!)
                }
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.days_list, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int = daysList.size

}