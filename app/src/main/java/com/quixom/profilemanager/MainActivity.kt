package com.quixom.profilemanager

import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AbsListView
import android.widget.Toast
import com.quixom.profilemanager.Adapter.ProfileAdapter
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Database.Profile
import com.quixom.profilemanager.Utils.Global
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    var database: MyDatabase? = null
    private var fetchProfileList: List<Profile>? = null
    private var fabExpanded: Boolean? = null
    private var mainIntent: Intent? = null
    private var doubleBackToExitPressedOnce: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        toolbarTitle.text = this.getString(R.string.app_name)

        database = Room.databaseBuilder(this, MyDatabase::class.java, "profile_database").allowMainThreadQueries().build()
        fabExpanded = false
        doubleBackToExitPressedOnce = false
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()

        closeSubMenusFab()
        floatingButton.setOnClickListener(this)
        layoutFabAddTimeProfile.setOnClickListener(this)
        layoutFabAddLocationProfile.setOnClickListener(this)
        shadowView.setOnClickListener(this)

        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 || dy < 0 && floatingButton.isShown) {
                    floatingButton.hide()
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE)
                    floatingButton.show()
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.floatingButton -> {
                when (fabExpanded) {
                    true -> {
                        closeSubMenusFab()
                    }
                    false -> {
                        openSubMenusFab()
                    }
                }
            }
            R.id.layoutFabAddTimeProfile -> {
                mainIntent = Intent(applicationContext, AddProfileActivity::class.java)
                mainIntent?.putExtra("comeFrom", "Add")
                mainIntent?.putExtra("profileOption", "Time")
                startActivity(mainIntent)
            }
            R.id.layoutFabAddLocationProfile -> {
                mainIntent = Intent(applicationContext, AddProfileActivity::class.java)
                mainIntent?.putExtra("comeFrom", "Add")
                mainIntent?.putExtra("profileOption", "Location")
                startActivity(mainIntent)
            }
            R.id.shadowView -> closeSubMenusFab()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.e(Global.TAG, "onResume called!!!")
        closeSubMenusFab()
        fetchProfile(database!!)
    }


    private fun fetchProfile(db: MyDatabase) {
        fetchProfileList = db.profileDao().getAllProfile()
        Log.e("Profile Manager", "fetch Profile List Size:::" + fetchProfileList!!.size)
        if (fetchProfileList?.isNotEmpty()!!) {
            linearLayoutRecyclerView?.visibility = View.VISIBLE
            linearLayoutNoProfiles?.visibility = View.GONE
            recyclerView?.adapter = ProfileAdapter(this, fetchProfileList as ArrayList<Profile>, database!!)
            recyclerView?.adapter?.notifyDataSetChanged()
        } else {
            linearLayoutRecyclerView?.visibility = View.GONE
            linearLayoutNoProfiles?.visibility = View.VISIBLE
        }
    }

    private fun closeSubMenusFab() {
        layoutFabAddTimeProfile.visibility = View.GONE
        layoutFabAddLocationProfile.visibility = View.GONE
        shadowView.visibility = View.GONE
        fabExpanded = false
    }

    private fun openSubMenusFab() {
        layoutFabAddTimeProfile.visibility = View.VISIBLE
        layoutFabAddLocationProfile.visibility = View.VISIBLE
        shadowView.visibility = View.VISIBLE
        fabExpanded = true
        doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (this.doubleBackToExitPressedOnce!!) {
            super.onBackPressed()
        } else {
            closeSubMenusFab()
            doubleBackToExitPressedOnce = true
            Toast.makeText(this@MainActivity, "Press again to exit!", Toast.LENGTH_LONG).show()
        }
    }

}
