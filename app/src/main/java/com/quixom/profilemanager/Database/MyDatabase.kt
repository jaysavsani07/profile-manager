package com.quixom.profilemanager.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters


/**
 * Created by Pooja on 28-12-2017.
 */
@Database(entities = arrayOf(Profile::class), version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class MyDatabase : RoomDatabase() {

//    private var INSTANCE: MyDatabase? = null

    abstract fun profileDao(): ProfileDao
}