package com.quixom.profilemanager.Adapter

import android.content.Context
import android.graphics.PorterDuff
import android.media.AudioManager
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quixom.profilemanager.AddProfileActivity
import com.quixom.profilemanager.Interface.ImagePosInterface
import com.quixom.profilemanager.R
import kotlinx.android.synthetic.main.ringermode_list.view.*


/**
 * Created by Pooja on 19-12-2017.
 */
class RingerModeAdapter(internal var context: Context, private var activity: AddProfileActivity,
                        private var ringerModeArrayList: ArrayList<Int>?,
                        private var imagePosInterface: ImagePosInterface, private var ringerModePos: Int? = null)
    : RecyclerView.Adapter<RingerModeAdapter.MyViewHolder>() {


    private var audioManager: AudioManager? = null
    private var lastSelectedImg: Int? = null
    private var imageId: Int? = null
    private var iconClicked: Boolean = false

    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.ringermode_list, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Log.e("PROFILE MANAGER", "onBindView ringermode called!!!!!")

        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        val imageResource = ringerModeArrayList!![position]
        Log.e("Profile Manager", "Image Resource:::" + ringerModeArrayList!![position])
        holder.itemView?.imageViewProfileRingerMode?.setImageResource(imageResource)
        setId(position + 1)
        val id = getId()
        Log.e("PROFILE MANAGER", "Id:::" + id)

        val mDrawable = ContextCompat.getDrawable(activity, imageResource)

        println("ringermode pos === $ringerModePos")

        if (!iconClicked) {

            if (ringerModePos != null && (position + 1) == ringerModePos) {
                val color = ContextCompat.getColor(activity, R.color.iconSelected)
                mDrawable!!.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                holder.itemView?.imageViewProfileRingerMode?.setImageDrawable(mDrawable)
                imagePosInterface.getImagePosition(this.ringerModePos!!)
            } else {
                val color = ContextCompat.getColor(activity, R.color.IconUnselected)
                mDrawable!!.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                holder.itemView?.imageViewProfileRingerMode?.setImageDrawable(mDrawable)
            }
        } else {
            if (id == lastSelectedImg) {
                val color = ContextCompat.getColor(activity, R.color.iconSelected)
                mDrawable!!.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                holder.itemView?.imageViewProfileRingerMode?.setImageDrawable(mDrawable)
            } else {
                val color = ContextCompat.getColor(activity, R.color.IconUnselected)
                mDrawable!!.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                holder.itemView?.imageViewProfileRingerMode?.setImageDrawable(mDrawable)
            }
        }

        holder.itemView?.imageViewProfileRingerMode?.setOnClickListener({
            iconClicked = true
            lastSelectedImg = position + 1
            Log.e("Profile Manager", "LastSelectedImage:::" + lastSelectedImg)
            /* var mDrawable = context.getResources().getDrawable(imageResource)*/
            Log.e("Profile Manager", "ImageResource:::" + imageResource)

            val color = ContextCompat.getColor(activity, R.color.iconSelected)
            mDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN)
            holder.itemView.imageViewProfileRingerMode?.setImageDrawable(mDrawable)

            imagePosInterface.getImagePosition(lastSelectedImg!!)
            notifyDataSetChanged()

        })
    }

    override fun getItemCount(): Int = ringerModeArrayList!!.size

    private fun setId(id: Int) {
        imageId = id
    }

    private fun getId(): Int {
        return this.imageId!!
    }
}