package com.quixom.profilemanager.Utils

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Service
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.media.AudioManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Database.Profile

class GPSTracker(private var context: Context?) : Service(), LocationListener {

    constructor() : this(context = null)

    init {
        if (context == null) {
            context = this
        }
    }

    private var isGPSEnabled = false

    private var isNetworkEnabled = false

    private var canGetLocation = false
    private val MIN_DISTANCE_FOR_UPDATE: Long = 0 //1 meter
    private val MIN_TIME_FOR_UPDATE: Long = 0 //5 seconds 1000 * 5 * 1
    private var PROFILE_REQUEST_CODE = 112

    var location: Location? = null
    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()
    private var locationManager: LocationManager? = null

    var database: MyDatabase? = null
    private var profile: Profile? = null
    private var profileId: Int? = null
    private var audioManager: AudioManager? = null
    private var locationProfiles: List<Profile>? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        database = Room.databaseBuilder(context!!, MyDatabase::class.java, "profile_database").allowMainThreadQueries().build()
        profile = Profile()

        audioManager = context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager?

        getProfileLocation()
        if (canGetLocation() != null) {
            if (intent != null) {
                profileId = intent.extras.getInt("profileId")
            }
        } else {
            showSettingsAlert()
        }

        return Service.START_STICKY
    }

    fun getProfileLocation(): Location? {
        Log.e(Global.TAG, "getLocation called!!!!!!")
        try {

            locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            isGPSEnabled = locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)!!

            isNetworkEnabled = locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER)!!

            if (!isGPSEnabled || !isNetworkEnabled) {
                Log.e(Global.TAG, "no network provider is enabled")
            } else {
                if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED /*&& ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED*/) {
                    Log.e(Global.TAG, "check self permission")
                    ActivityCompat.requestPermissions(context as Activity, arrayOf(/*Manifest.permission.ACCESS_COARSE_LOCATION,*/
                            Manifest.permission.ACCESS_FINE_LOCATION/*, Manifest.permission.ACCESS_NETWORK_STATE*/),
                            PROFILE_REQUEST_CODE)

                } else {
                    this.canGetLocation = true

                    if (isNetworkEnabled) {
                        locationManager?.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_FOR_UPDATE,
                                MIN_DISTANCE_FOR_UPDATE.toFloat(), this)
                        if (locationManager != null) {

                            location = locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                            if (location != null) {
                                latitude = location?.latitude!!
                                longitude = location?.longitude!!
                            }
                        }
                    }
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager?.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_FOR_UPDATE,
                                    MIN_DISTANCE_FOR_UPDATE.toFloat(), this)
                            if (locationManager != null) {
                                location = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                if (location != null) {
                                    latitude = location?.latitude!!
                                    longitude = location?.longitude!!
                                }
                            }
                        }
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        Log.e(Global.TAG, "return location:::" + location)
        return location
    }

    fun getLatitude(): Double? {
        if (location != null) {
            latitude = location?.latitude!!
        }
        return latitude
    }

    fun getLongitude(): Double? {
        if (location != null) {
            longitude = location?.longitude!!
        }
        return longitude
    }

    fun canGetLocation(): Boolean {
        return this.canGetLocation
    }

    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("GPS Settings")
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?")
        alertDialog.setPositiveButton("Settings", { _, _ ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            context?.startActivity(intent)
        })
        /* alertDialog.setNegativeButton("Cancel", { dialog, which ->
             dialog.cancel()

         })*/
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    fun stopUsingGPS() {
        if (locationManager != null) {
            locationManager?.removeUpdates(this@GPSTracker)
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        throw UnsupportedOperationException("Not yet implemented") as Throwable
    }

    override fun onLocationChanged(location: Location?) {
        latitude = location?.latitude!!
        longitude = location.longitude

        locationProfiles = database?.profileDao()?.getOnLocationProfiles("Location", "On")

        if (locationProfiles != null) {
            for (index in locationProfiles!!.indices) {
                Log.e(Global.TAG, "location On Profile---" + locationProfiles!![index].profileName)

                Log.e(Global.TAG, "onLocationChanged called!!!")
//        if (/*profile != null && */profileId != null) {
//            profile = database?.profileDao()?.loadProfileById(profileId!!)

                val dist = FloatArray(1)
                Location.distanceBetween(locationProfiles!![index].profileLatitude!!, locationProfiles!![index].profileLongitude!!,
                        latitude, longitude, dist)
                Log.e(Global.TAG, "Distance between::" + dist[0] / 1000)

                val radius = "0." + locationProfiles!![index].profileLocationRadius
                if (dist[0] / 1000 < radius.toFloat()) {
                    Log.e(Global.TAG, "You are into the profile location!")
                    when (locationProfiles!![index].profileRingerMode) {
                        1 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT
                        3 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                        2 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                        else -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    }
                } else {
                    /*
                 val onTimeProfile = database?.profileDao()?.getOnTimeProfiles("Location", "On")
                 val timeProfile = onTimeProfile?.profileId
*/

                    /*  if (timeProfile != null && timeProfile > 0) {
                    val ringerMode = database?.profileDao()?.loadProfileById(timeProfile)?.profileRingerMode
                    when (ringerMode) {
                        1 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_SILENT
                        2 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                        3 -> audioManager?.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                        else -> audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    }
                } else {
                    audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL
                }*/

//                audioManager?.ringerMode = AudioManager.RINGER_MODE_NORMAL

                    Log.e(Global.TAG, "You are out of the profile location!")
                }
            }
        }
        /* } else {
             stopUsingGPS()
         }*/
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

        Log.e(Global.TAG, "provider enabled::" + provider)

    }

    override fun onProviderDisabled(provider: String?) {
        Log.e(Global.TAG, "provider disabled::" + provider)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Profile Manager", "Service is stopped!!!!")
//        stopUsingGPS()
        Log.e(Global.TAG, "profile Id:::::" + profileId)
        database?.profileDao()?.updateProfileStatus(profileId, "Off")
        stopSelf()
    }

}
