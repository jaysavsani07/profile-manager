package com.quixom.profilemanager

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.location.Geocoder
import android.media.AudioManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.quixom.profilemanager.Adapter.DaysAdapter
import com.quixom.profilemanager.Adapter.RingerModeAdapter
import com.quixom.profilemanager.Database.MyDatabase
import com.quixom.profilemanager.Database.Profile
import com.quixom.profilemanager.Interface.ImagePosInterface
import com.quixom.profilemanager.Interface.PositionInterface
import com.quixom.profilemanager.Utils.GPSTracker
import com.quixom.profilemanager.Utils.Global
import kotlinx.android.synthetic.main.activity_add_profile.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AddProfileActivity : AppCompatActivity(), View.OnClickListener {
    var ringerMode: Int? = null
    var daysList: HashMap<Int, String>? = null
    private var PROFILE_REQUEST_CODE = 112
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var buttonList: ArrayList<String>? = null
    private var ringerModeList: ArrayList<Int>? = null
    private val cal = Calendar.getInstance()!!
    private var startTimeListener: TimePickerDialog.OnTimeSetListener? = null
    private var endTimeListener: TimePickerDialog.OnTimeSetListener? = null
    private var startTime: String = ""
    private var endTime: String = ""
    private var address: String = ""
    private var audioManager: AudioManager? = null
    private var gps: GPSTracker? = null
    var database: MyDatabase? = null
    private var profileName: String = ""
    private var profile: Profile? = null
    private var profileId: Int? = null
    private var profileRadius: String? = null
    private var comeFrom: String? = null
    private var profileOption: String? = ""
    private var profileType: String? = ""
    var daysArrayList: ArrayList<String>? = null
    private var mMap: GoogleMap? = null
    private var mapViewLatLng: LatLng? = null
    private var addEditIntent: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_profile)

        setSupportActionBar(toolbar)
        val backArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back)
        backArrow?.setColorFilter(ContextCompat.getColor(this, R.color.fontColor), PorterDuff.Mode.SRC_ATOP)
        supportActionBar!!.setHomeAsUpIndicator(backArrow)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        toolbarTitle.text = this.getString(R.string.app_name)
        toolbar.setNavigationOnClickListener({
            super.onBackPressed()
        })

        database = Room.databaseBuilder(this, MyDatabase::class.java, "profile_database").allowMainThreadQueries().build()
        profile = Profile()
        addEditIntent = intent
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        gps = GPSTracker(this@AddProfileActivity)

        ringerModeList = ArrayList()
        buttonList = arrayListOf("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")

//        daysArrayList = ArrayList()

        val mLayoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewProfileDays?.layoutManager = mLayoutManager
        recyclerViewProfileDays?.itemAnimator = DefaultItemAnimator()
        recyclerViewRingerMode?.layoutManager = layoutManager
        recyclerViewRingerMode?.itemAnimator = DefaultItemAnimator()

        buttonAdd.setOnClickListener(this)
        buttonDelete.setOnClickListener(this)
        buttonCancel.setOnClickListener(this)
        textViewEditLocation.setOnClickListener(this)
        textViewSetStartTime.setOnClickListener(this)
        textViewSetEndTime.setOnClickListener(this)


        startTimeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            Log.e("PROFILE MANAGER", "START TIME::::" + cal.time)
            startTime = SimpleDateFormat("HH:mm:ss a", Locale.US).format(cal.time)
            Log.e("PROFILE MANAGER", "TIME::::" + startTime)
            textViewSetStartTime!!.text = startTime
        }
        endTimeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            Log.e("PROFILE MANAGER", "END TIME::::" + cal.time)
            endTime = SimpleDateFormat("HH:mm:ss a", Locale.US).format(cal.time)
            textViewSetEndTime!!.text = endTime
        }

        if (addEditIntent != null) {
            comeFrom = addEditIntent?.extras?.get("comeFrom") as String?
            profileOption = addEditIntent?.extras?.get("profileOption") as String?
            profileType = addEditIntent?.extras?.get("profileType") as String?
            Log.e(Global.TAG, "Come From:::" + comeFrom)
            if (comeFrom == "Edit") {

                buttonDelete.visibility = View.VISIBLE
                buttonCancel.visibility = View.GONE

                profileId = addEditIntent?.extras?.get("profileId") as Int
                profile = database?.profileDao()?.loadProfileById(profileId!!)

                val selectedDays = database?.profileDao()?.loadProfileById(profileId!!)
                val fetchDaysById = selectedDays?.profileDays
                recyclerViewProfileDays?.adapter = DaysAdapter(applicationContext, database!!, Global.weekDaysList, fetchDaysById, "Edit", object : PositionInterface {
                    override fun getPositionArray(daysList: ArrayList<String>) {
                        daysArrayList = daysList
                        Log.e(Global.TAG, "days array list::::" + daysArrayList)
                    }

                    override fun getPositionHashMap(hashMap: HashMap<Int, String>) {
                        daysList = hashMap
                        Log.e(Global.TAG, "Interface days selected::" + daysList)
                    }
                })
                editTextAddProfileName.setText(profile?.profileName)
                editTextAddProfileName.setSelection(profile?.profileName?.length!!)

                if (profile?.profileType == "Time") {
                    linearLayoutTime.visibility = View.VISIBLE
                    linearLayoutLocation.visibility = View.GONE
                    textViewSetStartTime.text = profile?.profileStartDate
                    textViewSetEndTime.text = profile?.profileEndDate
                } else {
                    linearLayoutTime.visibility = View.GONE
                    linearLayoutLocation.visibility = View.VISIBLE
                    mapViewAddProfile.onCreate(null)
                    mapViewAddProfile.onResume()
                    mapViewAddProfile.getMapAsync({ googleMap: GoogleMap? ->
                        run {
                            mMap = googleMap
                            MapsInitializer.initialize(this)
                            mMap?.uiSettings?.isMapToolbarEnabled = false
                            mMap?.uiSettings?.isZoomControlsEnabled = false
                            mapViewLatLng = LatLng(profile?.profileLatitude!!, profile?.profileLongitude!!)
                            mMap?.addMarker(MarkerOptions().position(this.mapViewLatLng!!))
                            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mapViewLatLng, 13.0f))
                            mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))

                        }
                    })
                    mapViewAddProfile.postInvalidate()
                    editTextRadius.setText(profile?.profileLocationRadius)
                }

                Log.e("Profile Manager", "Ringer Mode:::" + profile?.profileRingerMode)
                when (profile?.profileRingerMode) {
                    1 -> {
                        ringerModeList = arrayListOf(R.drawable.ic_volume_off_blue, R.drawable.ic_volume_up, R.drawable.ic_vibration)
                    }
                    2 -> {
                        ringerModeList = arrayListOf(R.drawable.ic_volume_off, R.drawable.ic_volume_up_blue, R.drawable.ic_vibration)
                    }
                    3 -> {
                        ringerModeList = arrayListOf(R.drawable.ic_volume_off, R.drawable.ic_volume_up, R.drawable.ic_vibration_blue)
                    }
                }
                Log.e("Profile Manager", "Ringer Mode List-----" + ringerModeList)

                recyclerViewRingerMode!!.adapter = RingerModeAdapter(applicationContext, this, ringerModeList,
                        object : ImagePosInterface {
                            override fun getImagePosition(imageId: Int) {
                                ringerMode = imageId
                            }
                        }, profile?.profileRingerMode!!)

            } else {
                ringerModeList = arrayListOf(R.drawable.ic_volume_off, R.drawable.ic_volume_up, R.drawable.ic_vibration)
                buttonDelete.visibility = View.GONE
                buttonCancel.visibility = View.VISIBLE
                Log.e("Profile Manager", "Ringer Mode List:::::" + ringerModeList)
                if (profileOption == "Time") {
                    linearLayoutTime.visibility = View.VISIBLE
                    linearLayoutLocation.visibility = View.GONE
                } else {
                    checkPermission()
                    Log.e(Global.TAG, "Internet connection:::" + checkInternetConnection())
                    if (!checkInternetConnection()) {
                        Toast.makeText(this, "No Internet Connection. You need to have Mobile Data or Wifi to proceed!", Toast.LENGTH_LONG).show()
                    }
                    linearLayoutTime.visibility = View.GONE
                    linearLayoutLocation.visibility = View.VISIBLE
                }

                recyclerViewRingerMode?.adapter = RingerModeAdapter(applicationContext, this, ringerModeList,
                        object : ImagePosInterface {
                            override fun getImagePosition(imageId: Int) {
                                ringerMode = imageId
                                Log.e("Profile Manager", "Image id::::" + imageId)
                            }
                        }, null)

                recyclerViewProfileDays?.adapter = DaysAdapter(applicationContext, database!!, Global.weekDaysList, null, "Add", object : PositionInterface {
                    override fun getPositionArray(daysList: ArrayList<String>) {
                        daysArrayList = daysList
                    }

                    override fun getPositionHashMap(hashMap: HashMap<Int, String>) {
                        daysList = hashMap
                    }
                })
            }
        }

    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.buttonAdd -> {

                profileName = editTextAddProfileName?.text?.trim().toString()
                profileRadius = editTextRadius?.text?.toString()
                startTime = textViewSetStartTime.text.toString()
                endTime = textViewSetEndTime.text.toString()
                if (comeFrom == "Edit" && daysArrayList == null) {
                    daysArrayList = database?.profileDao()?.loadProfileById(this.profileId!!)?.profileDays
                }
                if (comeFrom == "Edit" && profile?.profileType == "Location" && address.isBlank()) {
                    address = database?.profileDao()?.loadProfileById(this.profileId!!)?.profileLocation!!
                    latitude = database?.profileDao()?.loadProfileById(this.profileId!!)?.profileLatitude!!
                    longitude = database?.profileDao()?.loadProfileById(this.profileId!!)?.profileLongitude!!
                }

                if (comeFrom == "Edit") {
                    profileOption = profileType
                }

                when (profileOption) {
                    "Time" -> {
                        if (profileName.isBlank() || daysArrayList!!.size < 1 || ringerMode == null || startTime.isBlank()
                                || endTime.isBlank()) {
                            Toast.makeText(applicationContext, "Please enter all details for this Profile!", Toast.LENGTH_LONG).show()
                        } else {

                            profileName = editTextAddProfileName?.text?.trim().toString()

                            if (comeFrom == "Edit") {
                                profileId = addEditIntent?.extras?.get("profileId") as Int?
                                updateProfile(database, this.profileId, profileName, startTime,
                                        endTime, null, null, null, null,
                                        daysArrayList, ringerMode, "Time", profile?.profileStatus)
                            } else {
                                addProfileValues(database, profileName, startTime, endTime, null,
                                        null, null, null, daysArrayList!!, ringerMode, "Time", "Off")
                            }
                            finish()
                        }
                    }
                    "Location" -> {

                        if (profileName.isBlank() || daysArrayList!!.size < 1 || profileRadius.isNullOrBlank() || ringerMode == null || address.isBlank()) {
                            Toast.makeText(applicationContext, "Please enter all details for this Profile!", Toast.LENGTH_LONG).show()
                        } else {
                            if (comeFrom == "Edit") {
                                profileId = addEditIntent?.extras?.get("profileId") as Int?
                                updateProfile(database, this.profileId, profileName, null,
                                        null, address, latitude, longitude, profileRadius, daysArrayList,
                                        ringerMode, "Location", profile?.profileStatus)
                            } else {
                                addProfileValues(database, profileName, null, null, address,
                                        latitude, longitude, profileRadius, daysArrayList!!, ringerMode as Int, "Location", "Off")
                            }

                            finish()
                        }
                    }
                }
                /*isNullOrBlank:Returns `true` if this nullable char sequence is either `null` or empty
                 or consists solely of whitespace characters.*/

            }
            R.id.buttonDelete -> {

                val alertDialog = AlertDialog.Builder(this@AddProfileActivity)
                alertDialog.setMessage("Are you sure you want to delete this Profile?")
                        .setCancelable(true)
                        .setPositiveButton("Yes") { dialog, _ ->
                            deleteProfile(database, profileId)
                            dialog.cancel()
                            finish()
                        }
                        .setNegativeButton("No") { dialog, _ -> dialog.cancel() }
                val dialog = alertDialog.create()
                dialog.show()

            }
            R.id.buttonCancel -> finish()
            R.id.textViewEditLocation -> {

                try {
                    val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this)
                    startActivityForResult(intent, 1)
                } catch (e: GooglePlayServicesRepairableException) {

                } catch (e: GooglePlayServicesNotAvailableException) {

                }
            }
            R.id.textViewSetStartTime -> {
                Global.hideKeyboard(this@AddProfileActivity)
                TimePickerDialog(this, R.style.DialogTheme, startTimeListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),
                        false).show()
            }
            R.id.textViewSetEndTime -> {
                Global.hideKeyboard(this@AddProfileActivity)
                TimePickerDialog(this, R.style.DialogTheme, endTimeListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),
                        false).show()
            }
        }
    }

    private fun addProfileValues(database: MyDatabase?, profileName: String?, profileStartTime: String?, profileEndTime: String?,
                                 profileLocation: String?, profileLatitude: Double?, profileLongitude: Double?, profileRadius: String?,
                                 profileDaysList: ArrayList<String>, profileRingerMode: Int?, profileType: String?, profileStatus: String?) {
        val profileInstance = Profile()
        profileInstance.profileName = profileName
        profileInstance.profileStartDate = profileStartTime
        profileInstance.profileEndDate = profileEndTime
        profileInstance.profileLocation = profileLocation
        profileInstance.profileLatitude = profileLatitude
        profileInstance.profileLongitude = profileLongitude
        profileInstance.profileLocationRadius = profileRadius
        profileInstance.profileDays = profileDaysList
        profileInstance.profileRingerMode = profileRingerMode
        profileInstance.profileType = profileType
        profileInstance.profileStatus = profileStatus
        database?.profileDao()?.insertProfile(profileInstance)
    }


    private fun updateProfile(db: MyDatabase?, profileId: Int?, profileName: String?, profileStartTime: String?,
                              profileEndTime: String?, profileLocation: String?, profileLatitude: Double?,
                              profileLongitude: Double?, profileRadius: String?, profileDays: ArrayList<String>?,
                              profileRingerMode: Int?, profileType: String?, profileStatus: String?) {
        db?.profileDao()?.updateProfile(profileId, profileName, profileStartTime, profileEndTime, profileLocation,
                profileLatitude, profileLongitude, profileRadius, profileDays, profileRingerMode, profileType, profileStatus)
    }

    private fun deleteProfile(db: MyDatabase?, pos: Int?) {
        db?.profileDao()?.deleteProfileById(pos!!)
    }

    private fun checkInternetConnection(): Boolean {
        val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    private fun checkPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(applicationContext,
                            /*arrayOf(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, ACCESS_NETWORK_STATE).toString()*/
                            android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e("PROFILE MANAGER", "Permission not granted!!!")
                ActivityCompat.requestPermissions(this, arrayOf(/*ACCESS_COARSE_LOCATION, */ACCESS_FINE_LOCATION
                        /*,ACCESS_NETWORK_STATE*/), PROFILE_REQUEST_CODE)
            } else {
                gps?.getProfileLocation()
                if (gps?.canGetLocation()!!) {
                    latitude = gps?.getLatitude()!!
                    longitude = gps?.getLongitude()!!
                    Log.e("PROFILE MANAGER", "Latitude and Longitude:::$latitude $longitude")
                    address = getAddress(latitude, longitude)
                    mapViewAddProfile.onCreate(null)
                    mapViewAddProfile.onResume()
                    mapViewAddProfile.getMapAsync({ googleMap: GoogleMap? ->
                        run {
                            mMap = googleMap
                            MapsInitializer.initialize(this)
                            mMap?.uiSettings?.isMapToolbarEnabled = false
                            mMap?.uiSettings?.isZoomControlsEnabled = false
                            if (profileOption == "Location" || profileType == "Location") {

                                mapViewLatLng = LatLng(latitude, longitude)
                                mMap?.addMarker(MarkerOptions().position(this.mapViewLatLng!!))
                                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mapViewLatLng, 13.0f))
                                mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))
                            }
                        }
                    })
                    mapViewAddProfile.postInvalidate()
//                    textViewLocation!!.text = address
                } else {
                    gps?.showSettingsAlert()
                }
            }
        } else {
            gps?.getProfileLocation()
            if (gps?.canGetLocation()!!) {
                latitude = gps?.getLatitude()!!
                longitude = gps?.getLongitude()!!
                Log.e("PROFILE MANAGER", "Latitude and Longitude:::$latitude $longitude")
                address = getAddress(latitude, longitude)
                mapViewAddProfile.onCreate(null)
                mapViewAddProfile.onResume()
                mapViewAddProfile.getMapAsync({ googleMap: GoogleMap? ->
                    run {
                        mMap = googleMap
                        MapsInitializer.initialize(this)
                        mMap?.uiSettings?.isMapToolbarEnabled = false
                        mMap?.uiSettings?.isZoomControlsEnabled = false
                        if (profileOption == "Location" || profileType == "Location") {

                            mapViewLatLng = LatLng(latitude, longitude)
                            mMap?.addMarker(MarkerOptions().position(this.mapViewLatLng!!))
                            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mapViewLatLng, 13.0f))
                            mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))
                        }
                    }
                })
                mapViewAddProfile.postInvalidate()
//                textViewLocation!!.text = address
            } else {
                gps!!.showSettingsAlert()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e("PROFILE MANAGER", "onRequestPermissionsResult called!!!")
        when (requestCode) {
            PROFILE_REQUEST_CODE -> {
                Log.e("Profile Manager", "case: PROFILE request code!!")
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /* currentLocation!!.enableGPS()*/
                    Log.e("PROFILE MANAGER", "Permission granted!!!")
                    Log.e("PROFILE MANAGER", "GPS canGetLocation:::" + gps!!.canGetLocation())
                    gps?.getProfileLocation()
                    if (gps?.canGetLocation()!!) {
                        latitude = gps?.getLatitude()!!
                        longitude = gps?.getLongitude()!!
                        Log.e("PROFILE MANAGER", "Latitude and Longitude:::$latitude $longitude")
                        address = getAddress(latitude, longitude)
                        if (comeFrom == "Add") {
                            mapViewAddProfile.onCreate(null)
                            mapViewAddProfile.onResume()
                            mapViewAddProfile.getMapAsync({ googleMap: GoogleMap? ->
                                run {
                                    mMap = googleMap
                                    MapsInitializer.initialize(this)
                                    mMap?.uiSettings?.isMapToolbarEnabled = false
                                    mMap?.uiSettings?.isZoomControlsEnabled = false
                                    if (profileOption == "Location") {

                                        mapViewLatLng = LatLng(latitude, longitude)
                                        mMap?.addMarker(MarkerOptions().position(this.mapViewLatLng!!))
                                        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mapViewLatLng, 13.0f))
                                        mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))
                                    }
                                }
                            })
                            mapViewAddProfile.postInvalidate()
                        }
                    }
                    /* else {
                         gps!!.showSettingsAlert()
                     }*/
                } else {
                    Log.e("PROFILE MANAGER", "Grant Results size::" + grantResults.size)
                }
            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(applicationContext, Locale.getDefault())
        var add: String? = ""
        try {
            if (lat != 0.0 && lng != 0.0) {
                val addresses = geocoder.getFromLocation(lat, lng, 1)
                val obj = addresses[0]
                add = obj.getAddressLine(0)
                add = add + "\n" + obj.countryName
                add = add + "\n" + obj.countryCode
                add = add + "\n" + obj.adminArea
                add = add + "\n" + obj.postalCode
                add = add + "\n" + obj.subAdminArea
                add = add + "\n" + obj.locality
                add = add + "\n" + obj.subThoroughfare

                Log.e("Profile Manager", "Address" + add)

            }
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
        return add!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        Log.e(Global.TAG, "requestCode::resultCode::data" + requestCode + resultCode + data)
        if (requestCode == 1) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    // retrive the data by using getPlace() method.
                    val place = PlaceAutocomplete.getPlace(this, data)
                    Log.e("GOOGLE PLACES", "Place: " + place.name + " " + place.address +
                            place.phoneNumber + " " + place.websiteUri)

                    val latLng = place.latLng
                    Log.e("GOOGLE PLACES", "Place-Latitude:::" + latLng.latitude)
                    Log.e("GOOGLE PLACES", "Place-Longitude:::" + latLng.longitude)
                    latitude = latLng.latitude
                    longitude = latLng.longitude
                    val placeAddress = place.name.toString() + " " + place.address
                    address = placeAddress
//                    mapViewLatLng = LatLng(latitude, longitude)
                    mapViewAddProfile.onCreate(null)
                    mapViewAddProfile.onResume()
                    mapViewAddProfile.getMapAsync({ googleMap: GoogleMap? ->
                        run {
                            mMap = googleMap
                            MapsInitializer.initialize(this)
                            mMap?.uiSettings?.isMapToolbarEnabled = false
                            mMap?.uiSettings?.isZoomControlsEnabled = false
//                            if (profileOption == "Location") {

                            mapViewLatLng = LatLng(latitude, longitude)
                            mMap?.addMarker(MarkerOptions().position(latLng))
                            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(mapViewLatLng, 13.0f))
                            mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json))
//                            }
                        }
                    })
                    mapViewAddProfile.postInvalidate()
//                    textViewLocation?.text = placeaddress

                }
                PlaceAutocomplete.RESULT_ERROR -> {
                    val status = PlaceAutocomplete.getStatus(this, data)
                    Log.e("GOOGLE PLACES", status.statusMessage)

                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
        }
    }

}

